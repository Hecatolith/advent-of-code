import heapq
from dataclasses import dataclass


@dataclass(frozen=True)
class State:
    energy: int
    rooms: tuple
    hallway: tuple = (None,) * 11

    def __lt__(self, other):
        return self.energy < other.energy

    @property
    def fingerprint(self):
        return self.hallway, self.rooms

    @property
    def is_done(self):
        return all(h is None for h in self.hallway) and all(
            all(a == i for a in room) for i, room in enumerate(self.rooms)
        )


def insert(tpl, i, new):
    return tpl[:i] + (new,) + tpl[i + 1:]


def prepare_data(data, test=False):
    return tuple(
        (ord(data[3][2 * i + 3]) - ord("A"), ord(data[2][2 * i + 3]) - ord("A"),)
        for i in range(4)
    )


def resu1(data):
    end = (2, 4, 6, 8)

    room_size = len(data[0])
    todo = [State(0, data)]
    visited = set()
    while todo:
        state = heapq.heappop(todo)
        if state.is_done:
            return state.energy
        if state.fingerprint in visited:
            continue
        visited.add(state.fingerprint)
        for ri, room in enumerate(state.rooms):
            if room and not all(a == ri for a in room):
                a = room[-1]
                for to, d in ((-1, -1), (11, 1)):
                    for hi in range(end[ri] + d, to, d):
                        if hi in end:
                            continue
                        if state.hallway[hi] is not None:
                            break
                        new = State(
                            state.energy
                            + (room_size - len(room) + 1 + abs(end[ri] - hi))
                            * (10 ** a),
                            insert(state.rooms, ri, room[:-1]),
                            insert(state.hallway, hi, a),
                        )
                        if new.fingerprint not in visited:
                            heapq.heappush(todo, new)
        for i, a in enumerate(state.hallway):
            if a is None:
                continue
            if i < end[a] and any(
                    u is not None for u in state.hallway[i + 1: end[a]]
            ):
                continue
            if i > end[a] and any(
                    u is not None for u in state.hallway[end[a] + 1: i]
            ):
                continue
            if any(u != a for u in state.rooms[a]):
                continue
            new = State(
                state.energy
                + (room_size - len(state.rooms[a]) + abs(end[a] - i)) * (10 ** a),
                insert(state.rooms, a, (state.rooms[a] + (a,))),
                insert(state.hallway, i, None),
            )
            if new.fingerprint not in visited:
                heapq.heappush(todo, new)


def resu2(data):
    data = (
        (data[0][0], 3, 3, data[0][1]),
        (data[1][0], 1, 2, data[1][1]),
        (data[2][0], 0, 1, data[2][1]),
        (data[3][0], 2, 0, data[3][1]),
    )
    return resu1(data)


def test1(resu):
    return resu == 12521


def test2(resu):
    return resu == 44169


if __name__ == "__main__":
    import os
    import sys

    day = int(os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0])
    year = int(os.path.basename(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    from utils import run

    run(day=day, year=year, bypasstest=False)
