from copy import deepcopy


def prepare_data(data, test=False):
    position = [0, 0]
    for line in data:
        if "Player 1 starting position:" in line:
            position[0] = int(line.strip("Player 1 starting position: ")) - 1
        elif "Player 2 starting position:" in line:
            position[1] = int(line.strip("Player 2 starting position: ")) - 1
    return position


def roll():
    global die
    die += 1
    return die


def resu1(data):
    position = deepcopy(data)
    global die
    die = 0

    score = [0, 0]
    player = 1
    while max(score) < 1000:
        player = (player + 1) % 2
        position[player] = (position[player] + roll() + roll() + roll()) % 10
        score[player] += position[player] + 1
    return min(score) * die


DP = {}


def count_win(p1, p2, s1, s2):
    global DP
    if s1 >= 21:
        return 1, 0
    if s2 >= 21:
        return 0, 1
    if (p1, p2, s1, s2) in DP:
        return DP[(p1, p2, s1, s2)]
    ans = (0, 0)
    for d1 in [1, 2, 3]:
        for d2 in [1, 2, 3]:
            for d3 in [1, 2, 3]:
                new_p1 = (p1 + d1 + d2 + d3) % 10
                new_s1 = s1 + new_p1 + 1

                x1, y1 = count_win(p2, new_p1, s2, new_s1)
                ans = (ans[0] + y1, ans[1] + x1)
    DP[(p1, p2, s1, s2)] = ans
    return ans


def resu2(data):
    global DP
    DP = {}
    return max(count_win(data[0], data[1], 0, 0))


def test1(resu):
    return resu == 739785


def test2(resu):
    return resu == 444356092776315


if __name__ == "__main__":
    import os
    import sys

    day = int(os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0])
    year = int(os.path.basename(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    from utils import run

    run(day=day, year=year, bypasstest=False)
