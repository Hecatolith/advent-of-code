def prepare_data(data, test=False):
    return data


def resu1(data):
    R = len(data)
    C = len(data[0])

    t = 0
    while True:
        t += 1
        moved = False
        G2 = [[data[r][c] for c in range(C)] for r in range(R)]
        for r in range(R):
            for c in range(C):
                if data[r][c] == '>':
                    if data[r][(c + 1) % C] == '.':
                        moved = True
                        G2[r][(c + 1) % C] = '>'
                        G2[r][c] = '.'
        G3 = [[G2[r][c] for c in range(C)] for r in range(R)]
        for r in range(R):
            for c in range(C):
                if G2[r][c] == 'v' and G2[(r + 1) % R][c] == '.':
                    moved = True
                    G3[(r + 1) % R][c] = 'v'
                    G3[r][c] = '.'
        if not moved:
            return t

        data = G3


def resu2(data):
    return "Happy Xmas"


def test1(resu):
    return resu == 58


def test2(resu):
    return True


if __name__ == "__main__":
    import os
    import sys

    day = int(os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0])
    year = int(os.path.basename(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    from utils import run

    run(day=day, year=year, bypasstest=False)
