def padd(x, y):
    return [a + b for a, b in zip(x, y)]


def pneg(v):
    return [-i for i in v]


def psub(x, y):
    return [a - b for a, b in zip(x, y)]


def pdist1(x, y=None):
    if y is not None:
        x = psub(x, y)
    return sum(map(abs, x))


def matmat(a, b):
    n, k1 = len(a), len(a[0])
    k2, m = len(b), len(b[0])
    assert k1 == k2
    out = [[None] * m for _ in range(n)]
    for i in range(n):
        for j in range(m):
            out[i][j] = sum(a[i][k] * b[k][j] for k in range(k1))
    return out


def matvec(a, v):
    return [j for i in matmat(a, [[x] for x in v]) for j in i]


def cross(a, b):
    return [a[1] * b[2] - a[2] * b[1],
            a[2] * b[0] - a[0] * b[2],
            a[0] * b[1] - a[1] * b[0]]


def get_scanners(scanners):
    facings = [x for i in [-1, 1] for x in [[i, 0, 0], [0, i, 0], [0, 0, i]]]

    def common(a, b):

        aset = set(map(tuple, a))
        # return b's points, but now relative to a
        for facing in facings:
            for up in [f for f in facings if all(abs(x) != abs(y) for x, y in zip(f, facing) if x or y)]:

                # facing's
                right = cross(facing, up)

                matrix = [facing, up, right]
                new_b = [matvec(matrix, vec) for vec in b]

                for a_point in a:
                    for b_point in new_b:
                        # assume they're the same
                        # add a-b to all b
                        delta = padd(a_point, pneg(b_point))
                        new_new_b = [padd(delta, b) for b in new_b]
                        if len(aset.intersection(map(tuple, new_new_b))) >= 12:
                            return new_new_b, delta
        return None

    good_scanners = [None] * len(scanners)
    good_scanners[0] = scanners[0]
    done_scanners = [False] * len(scanners)
    deltas = [None] * len(scanners)
    deltas[0] = [0, 0, 0]
    while True:
        for i in range(len(scanners)):
            if good_scanners[i] and not done_scanners[i]:
                for j in range(len(scanners)):
                    if i != j and good_scanners[j] is None:
                        test = common(good_scanners[i], scanners[j])
                        if test is not None:
                            good_scanners[j] = test[0]
                            deltas[j] = test[1]
                done_scanners[i] = True
        if all(done_scanners):
            break

    return set(tuple(point) for points in good_scanners for point in points), deltas


def prepare_data(data, test=False):
    resu = list()
    first = True
    temp = list()
    for line in data:
        if first:
            first = False
            continue
        if line == "":
            first = True
            resu.append(temp)
            temp = list()
            continue
        temp.append(list(map(int, line.split(','))))
    if temp != []:
        resu.append(temp)
    return get_scanners(resu)


def resu1(data):
    beacons, _ = data
    return len(beacons)


def resu2(data):
    _, deltas = data

    resu = 0
    for x in deltas:
        for y in deltas:
            resu = max(resu, pdist1(x, y))

    return resu


def test1(resu):
    return resu == 79


def test2(resu):
    return resu == 3621


if __name__ == "__main__":
    import os
    import sys

    day = int(os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0])
    year = int(os.path.basename(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    from utils import run

    run(day=day, year=year, bypasstest=False)
