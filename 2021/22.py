from collections import defaultdict
from tqdm import tqdm


def prepare_data(data, test=False):
    if test:
        i = data.index('')
        return test, [prepare_data(data[:i])[1], prepare_data(data[i + 1:])[1]]
    new_data = list()
    for line in data:
        switch, coord = line.split()
        x, y, z = coord.split(',')
        x, y, z = list(map(lambda s: int(s), x[2:].split(".."))), list(map(lambda s: int(s), y[2:].split(".."))), list(
            map(lambda s: int(s), z[2:].split("..")))
        new_data.append([switch == "on", [x, y, z]])
    return False, [new_data, data]


def resu1(data):
    test, data = data
    if test:
        data = data[0]
    data = data[0]
    temp = defaultdict(lambda: False)
    for switch, coord in data:
        x, y, z = coord
        for i in range(max(-50, x[0]), min(50, x[1]) + 1):
            for j in range(max(-50, y[0]), min(50, y[1]) + 1):
                for k in range(max(-50, z[0]), min(50, z[1]) + 1):
                    temp[(i, j, k)] = switch
    resu = 0
    for i in range(-50, 51):
        for j in range(-50, 51):
            for k in range(-50, 51):
                resu += temp[(i, j, k)]
    return resu


def resu2(data):
    test, data = data
    if test:
        data = data[1]
    data = data[1]

    return part2(data)

    X = set()
    Y = set()
    Z = set()
    C = []
    min_x = 0
    min_y = 0
    min_z = 0
    max_x = 0
    max_y = 0
    max_z = 0
    for cmd, coord in data:
        x, y, z = coord
        x1, x2 = min(x), max(x)
        y1, y2 = min(y), max(y)
        z1, z2 = min(z), max(z)

        X.add(x1)
        X.add(x2 + 1)
        Y.add(y1)
        Y.add(y2 + 1)
        Z.add(z1)
        Z.add(z2 + 1)

        min_x = min(x1, min_x)
        min_y = min(y1, min_y)
        min_z = min(z1, min_z)
        max_x = max(x2, max_x)
        max_y = max(y2, max_y)
        max_z = max(z2, max_z)
        C.append((x1, x2, y1, y2, z1, z2, cmd))

    def expand(A):
        B = set()
        for a in A:
            B.add(a)
        B = sorted(B)

        resu = {}
        U = {}
        len_sum = 0
        for i, b in enumerate(B):
            resu[b] = i
            if i + 1 < len(B):
                len_ = B[i + 1] - b if i + 1 < len(B) else None
                len_sum += len_
                U[i] = len_
        for a in A:
            assert a in resu
        assert len_sum == max(B) - min(B), f'{len_sum} {max(B) - min(B)}'
        return resu, U

    X.add(-50)
    X.add(51)
    Y.add(-50)
    Y.add(51)
    Z.add(-50)
    Z.add(51)

    X, UX = expand(X)
    Y, UY = expand(Y)
    Z, UZ = expand(Z)

    G = set()
    for x1, x2, y1, y2, z1, z2, on in tqdm(C, disable=test):
        for x in range(X[x1], X[x2 + 1]):
            for y in range(Y[y1], Y[y2 + 1]):
                for z in range(Z[z1], Z[z2 + 1]):
                    if on:
                        G.add((x, y, z))
                    else:
                        G.discard((x, y, z))

    resu = 0
    for x, y, z in tqdm(G, disable=test):
        resu += UX[x] * UY[y] * UZ[z]
    return resu


def test1(resu):
    return resu == 590784


def test2(resu):
    return resu == 2758514936282235


import copy


def parse(lines: str) -> list:
    ops = []
    for line in lines:
        actionst, rest = line.split()
        action = "on" in actionst
        coords = rest.strip().split(",")
        ranges = []
        for coord in coords:
            begs, ends = coord[2:].strip().split("..")
            beg, end = int(begs.strip()), int(ends.strip())
            ranges.append((beg, end))
        ops.append((action, tuple(ranges)))
    return ops


def part1(data: str) -> str:
    ops = parse(data)
    turned = set()

    def clamp(mi, ma):
        if mi > 50 or ma < -50:
            return None, None
        return max(-50, mi), min(50, ma)

    for action, ranges in ops:
        xmin, xmax = clamp(*ranges[0])
        ymin, ymax = clamp(*ranges[1])
        zmin, zmax = clamp(*ranges[2])
        if xmin is not None and ymin is not None and zmin is not None:
            for x in range(xmin, xmax + 1):
                for y in range(ymin, ymax + 1):
                    for z in range(zmin, zmax + 1):
                        if action:
                            turned.add((x, y, z))
                        elif (x, y, z) in turned:
                            turned.remove((x, y, z))
    return str(len(turned))


def part2(data: str) -> int:
    ops = parse(data)
    _, ranges = ops[0]
    turned = set()
    turned.add(ranges)
    for action, ranges in tqdm(ops[1:]):
        assert len(turned) != 0

        for cuboid in copy.deepcopy(turned):
            intersection = intersect(cuboid, ranges)
            if intersection:
                turned.remove(cuboid)
                externals = outof(ranges, cuboid)
                for external in externals:
                    turned.add(external)
        if action:
            turned.add(ranges)
    ans = 0
    for cuboid in tqdm(turned):
        ans += size(cuboid)
    return ans


def outof(c1, c2):
    ext_ranges = [[], [], []]
    for index, (c1r, c2r) in enumerate(zip(c1, c2)):
        c1min, c1max = c1r
        c2min, c2max = c2r
        if c2min < c1min:
            ext_ranges[index].append((c2min, min(c2max, c1min - 1)))
        if c2max > c1max:
            ext_ranges[index].append((max(c2min, c1max + 1), c2max))
    res = []
    fxr, fyr, fzr = c2
    sxr, syr, syz = c1
    for xr in ext_ranges[0]:
        res.append((xr, fyr, fzr))
    for yr in ext_ranges[1]:
        res.append((intersect_range(fxr, sxr), yr, fzr))
    for zr in ext_ranges[2]:
        res.append((intersect_range(fxr, sxr), intersect_range(fyr, syr), zr))
    return res


def intersect(c1, c2):
    intersection = []
    for c1r, c2r in zip(c1, c2):
        its = intersect_range(c1r, c2r)
        if its is None:
            return None
        intersection.append(its)
    return intersection


def intersect_range(r1, r2):
    mi = max(r1[0], r2[0])
    ma = min(r1[1], r2[1])
    if mi > ma:
        return None
    return mi, ma


def size(c):
    prod = 1
    for r in c:
        diff = r[1] - r[0] + 1
        prod *= diff
    return prod


if __name__ == "__main__":
    import os
    import sys

    day = int(os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0])
    year = int(os.path.basename(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    from utils import run

    run(day=day, year=year, bypasstest=False)
