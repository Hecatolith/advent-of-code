from itertools import permutations
from math import ceil
from functools import reduce


def add_left(x, n):
    if n is None:
        return x
    if isinstance(x, int):
        return x + n
    return [add_left(x[0], n), x[1]]


def add_right(x, n):
    if n is None:
        return x
    if isinstance(x, int):
        return x + n
    return [x[0], add_right(x[1], n)]


def explode(x, n=4):
    if isinstance(x, int):
        return False, None, x, None
    if n == 0:
        return True, x[0], 0, x[1]
    a, b = x
    exp, left, a, right = explode(a, n - 1)
    if exp:
        return True, left, [a, add_left(b, right)], None
    exp, left, b, right = explode(b, n - 1)
    if exp:
        return True, None, [add_right(a, left), b], right
    return False, None, x, None


def split(x):
    if isinstance(x, int):
        if x >= 10:
            return True, [x // 2, ceil(x / 2)]
        return False, x
    a, b = x
    change, a = split(a)
    if change:
        return True, [a, b]
    change, b = split(b)
    return change, [a, b]


def add(a, b):
    x = [a, b]
    while True:
        change, _, x, _ = explode(x)
        if change:
            continue
        change, x = split(x)
        if not change:
            break
    return x


def magnitude(x):
    if isinstance(x, int):
        return x
    return 3 * magnitude(x[0]) + 2 * magnitude(x[1])


def prepare_data(data, test=False):
    return list(map(eval, data))


def resu1(data):
    return magnitude(reduce(add, data))


def resu2(data):
    return max(magnitude(add(a, b)) for a, b in permutations(data, 2))


def test1(resu):
    return resu == 4140


def test2(resu):
    return resu == 3993


if __name__ == "__main__":
    import os
    import sys
    day = int(os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0])
    year = int(os.path.basename(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    from utils import run
    run(day=day, year=year, bypasstest=False)
