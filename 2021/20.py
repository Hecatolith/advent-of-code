from collections import defaultdict
from copy import deepcopy


def prepare_data(data, test=False):
    lines = [line.strip() for line in data]

    s = lines[0]

    size = [[0, len(lines)], [0, len(lines[2])]]
    grid = defaultdict(lambda: ".")
    for i in range(2, len(lines)):
        line = lines[i]
        for c in range(len(lines[i])):
            grid[(i - 2, c)] = line[c]
    part_1 = s, size, grid
    part_2 = deepcopy(part_1)
    return part_1, part_2


def compute(data, n=2):
    s, size, grid = data
    for i in range(n):
        new_grid = defaultdict(lambda: (s[-1], s[0])[i % 2])
        for k in grid:
            new_grid[k] = grid[k]
        size[0][0] -= 2
        size[0][1] += 2
        size[1][0] -= 2
        size[1][1] += 2
        for r in range(*size[0]):
            for c in range(*size[1]):
                bs = ""
                for dr in [-1, 0, 1]:
                    for dc in [-1, 0, 1]:
                        bs += grid[(r + dr, c + dc)]
                bs = bs.replace(".", "0")
                bs = bs.replace("#", "1")
                new_grid[(r, c)] = s[int(bs, 2)]
        grid = new_grid

    resu = 0
    for k in grid:
        resu += (grid[k] == "#")
    return resu


def resu1(data):
    part_1, _ = data
    return compute(part_1)


def resu2(data):
    _, part_2 = data
    return compute(part_2, 50)


def test1(resu):
    return resu == 35


def test2(resu):
    return resu == 16916


if __name__ == "__main__":
    import os
    import sys

    day = int(os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0])
    year = int(os.path.basename(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    from utils import run

    run(day=day, year=year, bypasstest=False)
