def prepare_data(data, test=False):
    if test:
        return None
    div_z = []
    add_x = []
    add_y = []

    for line_number, line in enumerate(data):
        op, *params = line.split(' ')
        if line_number % 18 == 4:
            div_z.append(int(params[1]))
        elif line_number % 18 == 5:
            add_x.append(int(params[1]))
        elif line_number % 18 == 15:
            add_y.append(int(params[1]))

    return div_z, add_x, add_y


def resu1(data):
    if data is None:
        return None

    div_z, add_x, add_y = data

    def process_digit(i, w, z):
        x = z % 26 + add_x[i]
        z = z // div_z[i]
        if x != w:
            z *= 26
            z += w + add_y[i]
        return z

    def solve(nums, z):
        if len(nums) == 14:
            return ''.join(map(str, nums))
        for num in range(9, 1 - 1, -1):
            new_z = process_digit(len(nums), num, z)
            pops_left = div_z[len(nums) + 1:].count(26)
            if new_z // 26 ** pops_left != 0:
                continue
            resu = solve(nums + [num], new_z)
            if resu is not None:
                return resu
        return None

    return solve([], 0)


def resu2(data):
    if data is None:
        return None

    div_z, add_x, add_y = data

    def process_digit(i, w, z):
        x = z % 26 + add_x[i]
        z = z // div_z[i]
        if x != w:
            z *= 26
            z += w + add_y[i]
        return z

    def solve(nums, z):
        if len(nums) == 14:
            return ''.join(map(str, nums))
        for num in range(1, 9 + 1):
            new_z = process_digit(len(nums), num, z)
            pops_left = div_z[len(nums) + 1:].count(26)
            if new_z // 26 ** pops_left != 0:
                continue
            resu = solve(nums + [num], new_z)
            if resu is not None:
                return resu
        return None

    return solve([], 0)


def test1(resu):
    return True


def test2(resu):
    return True


if __name__ == "__main__":
    import os
    import sys

    day = int(os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0])
    year = int(os.path.basename(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    from utils import run

    run(day=day, year=year, bypasstest=False)
